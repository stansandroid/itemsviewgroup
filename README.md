# ItemsViewGroup #

`ItemsViewGroup` is an Android `ViewGroup` to simplify creation of custom layouts with support of views recycling and gestures support like scrolling, flinging, etc. Views cache supports multiple views types, so your custom layouts can have different views and for each view type there will be a cache. 

The idea behind `ItemsViewGroup` is simple: encapsulate layout agnostic things like views recycling (similar to what `ListView` does) or common touch interactivity, and delegate a specific items layout to subclasses. 

# Samples #
Creating a custom layout on top of the `ItemsViewGroup` is fairly simple. Basically all you need to do is to implement `layoutItems` method and provide items data in some way (e.g. via `Adapter`). Optionally you may want to specify a scroll range, items padding, etc. 

As an example there is a [GridView implementation](https://bitbucket.org/NxAlex/itemsviewgroup/src/484f4aa4ef7cc1da42e4be38384f9a45cf845d4e/src/com/visionapps/demo/view/grid/GridView.java?at=master). `GridView` is a `ViewGroup` that displays items in a two-dimensional grid with horizontal and vertical scrolling. It behaves similarly to a default Android [GridView](http://developer.android.com/reference/android/widget/GridView.html), however in addition it is able to scroll in both directions (not only vertically).
![grid.png](https://bitbucket.org/repo/ardao8/images/3131834316-grid.png)

Also it supports rows and columns of different sizes. Each item view is measured to fit a grid cell.
![grid 2.png](https://bitbucket.org/repo/ardao8/images/503056642-grid%202.png)

There is a [video demonstration](https://vimeo.com/107820912) of the `GridView` in action.
Note: the video is recorded with the `adb shell screenrecord` command and the frame-rate is pretty low (~15 fps). So the video looks not so smooth as the view really is. We recommend to run the sample to see how fast and smooth it is. This repository contains [sample sources](https://bitbucket.org/NxAlex/itemsviewgroup/src) as well as a compiled [apk](https://bitbucket.org/NxAlex/itemsviewgroup/downloads/ItemsViewGroupDemo.apk).

# Extensibility #
`GridView` is easily extendable as well as `ItemsViewGroup`. Let's say we need to have a view with vertical scrolling and horizontal space evenly distributed among grid columns. So it looks like the default Android GridView. All we need to do is to override `getColumnWidth` method:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~ .java
    @Override
    protected int getColumnWidth(int column) {
        return getMeasuredWidth() == 0 ?
                super.getColumnWidth(column) :
                // calculate evenly distributed horizontal space with respect to padding
                (getMeasuredWidth() - itemPadding) / data.getColumnsCount() - itemPadding;
    }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

And voila, now we have a [VerticalGridView](https://bitbucket.org/NxAlex/itemsviewgroup/src/484f4aa4ef7cc1da42e4be38384f9a45cf845d4e/src/com/visionapps/demo/view/grid/VerticalGridView.java?at=master)!
![vertical.png](https://bitbucket.org/repo/ardao8/images/12938053-vertical.png) 


The same trick works for [HorizontalGridView](https://bitbucket.org/NxAlex/itemsviewgroup/src/0730029f8cfeaadaeeade476f5fe82d6c8875667/src/com/visionapps/demo/view/grid/HorizontalGridView.java?at=master) to have a view with horizontal scrolling and evenly distributed vertical space among grid rows.
![horizontal.png](https://bitbucket.org/repo/ardao8/images/1252815368-horizontal.png)

It's easy to create different types of layouts, not only grids and lists (which are essentially grids with just 1 column). For example, by having a variable number of columns per row we can have something interesting like:
![scrappy.png](https://bitbucket.org/repo/ardao8/images/1073333955-scrappy.png)

# Getting Help #
To report a specific problem or feature request, open a new issue on Bitbucket. For questions, suggestions, or anything else email me at [alex.naberezhnov@gmail.com](mailto:alex.naberezhnov@gmail.com)

# Developed By #

* Alexander Naberezhnov - [alex.naberezhnov@gmail.com](mailto:alex.naberezhnov@gmail.com)
     
#  #